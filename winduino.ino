#include <Arduino.h>

// TODO
// - Upload data to a server
// - Provide data through a web ui

/**
 * Defines the pins of the LEDs
 */
#define GREEN D0
#define YELLOW D2
#define RED D5

// With the current resistor, open window is 40
// and closed between 52 and 80. This is an indicator
// that I might want to get actual 10kOhm resistors.
#define THRESHOLD 50
#define MAX_SESSIONS 1000
// Keep 8 hours of sessions
#define TO_KEEP (60*60*8)

typedef struct
{
    int start;
    int duration;
} Session;

int currentIteration;
int isOpen;
int lastOpened;
int lastClosed;
Session openSessions[MAX_SESSIONS];
int sessionsHead = 0;
int sessionsTail = 0;

void setup()
{
    Serial.begin(9600);
    Serial.println("Hello!");

    pinMode(GREEN, OUTPUT);
    pinMode(YELLOW, OUTPUT);
    pinMode(RED, OUTPUT);
    pinMode(LED_BUILTIN, OUTPUT);

    currentIteration = 0;
    isOpen = 0;
    lastOpened = 0;
    lastClosed = 0;
    digitalWrite(LED_BUILTIN, HIGH);

    reset();
    digitalWrite(GREEN, HIGH);
    delay(200);
    digitalWrite(YELLOW, HIGH);
    delay(200);
    digitalWrite(RED, HIGH);
    delay(200);
}

/**
 * Resets the LEDs (turns them off).
 */
void reset()
{
    digitalWrite(GREEN, LOW);
    digitalWrite(YELLOW, LOW);
    digitalWrite(RED, LOW);
}

void addSession(int start)
{
    int duration = currentIteration-start;
    openSessions[sessionsTail].start = start;
    openSessions[sessionsTail].duration = duration;
    sessionsTail++;
    sessionsTail %= MAX_SESSIONS;
}

void cleanupSessions()
{
    int cutoff = currentIteration - TO_KEEP;
    if (cutoff < 0)
        return;
    for (int i = sessionsHead; i != sessionsTail;
        i = (i+1) % MAX_SESSIONS)
        if (openSessions[i].start + openSessions[i].duration >= cutoff)
        {
            sessionsHead = i;
            break;
        }
}

int getTotalOpen(int hours)
{
    int cutoff = currentIteration - 60*60*hours;
    int res = 0;
    if (isOpen)
        res += currentIteration - lastOpened;
    for (int i = sessionsHead; i != sessionsTail;
        i = (i+1) % MAX_SESSIONS)
        if (openSessions[i].start + openSessions[i].duration >= cutoff)
        {
            if (openSessions[i].start < cutoff)
                res += openSessions[i].start + openSessions[i].duration - cutoff;
            else
                res += openSessions[i].duration;
        }
    return res;
}

void update()
{
    reset();
    if (getTotalOpen(8) < 5*60 && getTotalOpen(4) < 3*60)
        digitalWrite(RED, HIGH);
    else if (getTotalOpen(4) < 5*60)
        digitalWrite(YELLOW, HIGH);
    else
        digitalWrite(GREEN, HIGH);
}

void loop()
{
    currentIteration++;
    cleanupSessions();
    update();
    int value = analogRead(A0);
    Serial.print("Measured value: ");
    Serial.println(value);
    if (value < THRESHOLD && !isOpen)
    {
        isOpen = 1;
        lastOpened = currentIteration;
        Serial.print("Opened the window! Window has been closed for ");
        Serial.print(currentIteration - lastClosed);
        Serial.println("s.");
        digitalWrite(LED_BUILTIN, LOW);
    }
    else if (value > THRESHOLD && isOpen)
    {
        isOpen = 0;
        lastClosed = currentIteration;
        Serial.print("Closed the window! Window has been opened for ");
        Serial.print(currentIteration - lastOpened);
        Serial.println("s.");
        addSession(lastOpened);
        Serial.print("Total open in 8 hours: ");
        Serial.print(getTotalOpen(8));
        Serial.println("s");
        digitalWrite(LED_BUILTIN, HIGH);
    }
    delay(1000);
}

