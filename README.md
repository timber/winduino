# Winduino

An application for measuring when you should open the window (without sensors except for a photo diode).
It works with three LEDs in a traffic signal kind of way:
- Red: The window was open for less than 3 minutes in the last 4 hours and less than 5 minutes in the last 8 hours
- Yellow: The window was open for less than 5 minutes in the last 4 hours
- Green: The window was open for at least 5 minutes in the last 4 hours

As an additional indicator, the onboard LED is lit up when the window is open.

The detection is done via a photoelectric barrier (working with an IR led and a photo diode) to detect a piece of paper attached to the window.
It thus knows when the window is open and can make the necessary measurements.

The photoelectric barrier is connected to A0 (between a resistor and the positive side of the diode), the LEDs are connected to D0 (green), D2 (yellow) and D5 (red).

Here is a schematic:

![A schematic displaying the described wiring](./schema.png)

Still in early stages of development, so expect better documentation at a later point in time.
